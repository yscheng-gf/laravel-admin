<?php

namespace Xn\Admin\Grid\Filter\Presenter;

use Xn\Admin\Facades\Admin;
use Illuminate\Contracts\Support\Arrayable;

class MultipleSelectTree extends Select
{
    protected $js = [
        '/vendor/laravel-admin/AdminLTE/plugins/combo-tree/comboTreePlugin.js',
        '/vendor/laravel-admin/AdminLTE/plugins/combo-tree/icontains.js'
    ];

    protected $css = [
        '/vendor/laravel-admin/AdminLTE/plugins/combo-tree/comboTreeStyle.css'
    ];

    protected function assets() {
        foreach ($this->js as $js) {
            Admin::js($js);
        }
        foreach ($this->css as $css) {
            Admin::css($css);
        }
    }


    /**
     * Build options.
     *
     * @return array
     */
    protected function buildOptions(): array
    {
        if (is_string($this->options)) {
            $this->loadRemoteOptions($this->options);
        }

        if ($this->options instanceof \Closure) {
            $this->options = $this->options->call($this->filter, $this->filter->getValue());
        }

        if ($this->options instanceof Arrayable) {
            $this->options = $this->options->toArray();
        }

        if (empty($this->script)) {

            $configs = array_merge([
                'isMultiple' => true,
                'cascadeSelect' => true,
                'selected' => explode(",", $_GET[$this->getElementClass()]??""),
            ], $this->config);

            $configs = json_encode($configs);
            $configs = substr($configs, 1, strlen($configs) - 2);

            $optionsJson = json_encode($this->options);

            $this->script = <<<SCRIPT
(function ($){
    var combo = $(".{$this->getElementClass()}").comboTree({
      source: $optionsJson,
      $configs
    });
    combo.onChange(function(){
        $('input[name="{$this->getElementClass()}"]').val(this.getSelectedIds());
        $('div.{$this->getElementClass()}').text(this.getSelectedNames());
    });
    $('div.{$this->getElementClass()}').text(combo.getSelectedNames());
})(jQuery);

SCRIPT;
        }

        $this->assets();

        Admin::script($this->script);

        return is_array($this->options) ? $this->options : [];
    }
}
