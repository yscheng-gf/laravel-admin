<?php

namespace Xn\Admin\Grid\Displayers;

use Illuminate\Support\Facades\Log;
use Xn\Admin\Admin;
use Illuminate\Contracts\Support\Renderable;

class Layer extends AbstractDisplayer
{
    /**
     * @param string $title
     * @param string $type
     * @param \Closure|string $callback
     * @param array $paramaters
     *
     * @return mixed|string
     */
    public function display()
    {
        $args = func_get_args();

        if ($args[0] instanceof \Closure) {
            $args[0] = $args[0]->call($this, $this->row);
        }
        if ($args[2] instanceof \Closure) {
            $args[2] = $args[2]->call($this, $this->row);
        }
        switch ($args[1]) {
            case "dialog":
                $layerType = 0;
                break;
            case "iframe":
                $layerType = 2;
                break;
            case "loading":
                $layerType = 4;
                break;
            case "tips":
                $layerType = 4;
                break;
            default:
                $layerType = 1;
        }
        $layerConfig = [
            'type' => $layerType,
            'area' => ['50%', '40%'],
        ];
        if (!empty($args[0])){
            $layerConfig['title'] = $args[0];
        }

        if (isset($args[3]) && is_array($args[3])) {
            $layerConfig = array_merge($layerConfig, $args[3]);
        }

        $layerConfig['content'] = $args[2];

        return Admin::component('admin::components.column-layer', [
            'layerConfig' => $layerConfig,
            'key'     => $this->getKey(),
            'value'   => $this->value,
        ]);
    }
}
