<?php

namespace Xn\Admin\Widgets;

use Illuminate\Contracts\Support\Renderable;
use Xn\Admin\Facades\Admin;

class LayerButton extends Widget implements Renderable
{

    protected $size = ['800px', '800px'];
    /**
     * @var string
     */
    protected $view = 'admin::widgets.layer-button';

    /**
     * @var string|\Symfony\Component\Translation\TranslatorInterface
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $style = 'default';

    protected $selector = '';

    protected $renderable = '';

    protected $func = '';

    protected $middleware = '';


    public function __construct(array $content, $title = '', $style = 'danger')
    {
        $this->renderable = $content[0];

        $this->func = $content[1];

        $this->middleware = $content[2]??null;

        $this->title = $title ?: trans('admin.alert');

        $this->style($style);

        $this->selector = uniqid('layer-link-');
    }

    /**
     * Size
     *
     * @param [type] $width
     * @param [type] $height
     * @return this
     */
    public function size($width, $height) {
        $this->size = [$width, $height];
        return $this;
    }

    /**
     * Add style.
     *
     * @param string $style
     *
     * @return $this
     */
    public function style($style = 'info')
    {
        $this->style = $style;

        return $this;
    }

    /**
     * @return array
     */
    protected function variables()
    {
        return [
            'title'      => $this->title,
            'content'    => $this->content,
            'style'      => $this->style,
            'selector'   => $this->selector,
            'attributes' => $this->formatAttributes(),
        ];
    }

    /**
     * @param int $multiple
     *
     * @return string
     */
    protected function getLoadUrl()
    {
        $renderable = str_replace('\\', '_', $this->renderable);

        $middleware = empty($this->middleware) ? null : str_replace('\\', '_', $this->middleware);

        return route('admin.handle-renderable', ['renderable' => $renderable, 'func' => $this->func, 'middleware' => $middleware]);
    }

    /**
     * @return mixed
     */
    protected function addScript()
    {
        $size = json_encode($this->size);
        $script = <<<SCRIPT

(function ($) {
    $('body').off('click', '.{$this->selector}').on('click', '.{$this->selector}', function() {
        layer.open({
            type: 2,
            area: {$size},
            title: '{$this->title}',
            shade: 0, // 遮罩透明度
            shadeClose: true, // 点击遮罩区域，关闭弹层
            maxmin: true, // 允许全屏最小化
            anim: 0, // 0-6 的动画形式，-1 不开启
            offset: 'auto',
            zIndex: layer.zIndex,
            content: '{$this->getLoadUrl()}',
            success: function(layero, index){
                // layer.iframeAuto(index); // 让 iframe 高度自适应
                layer.setTop(layero);
                layer.escIndex = layer.escIndex || [];
                layer.escIndex.unshift(index);
                layero.on('mousedown', function(){
                    var _index = layer.escIndex.indexOf(index);
                    if(_index !== -1){
                        layer.escIndex.splice(_index, 1);
                    }
                    layer.escIndex.unshift(index);
                });
            },
            end: function(){
                if(typeof layer.escIndex === 'object'){
                    layer.escIndex.splice(0, 1);
                }
            },
        });
    });
})(jQuery);

SCRIPT;

        Admin::script($script);
    }

    /**
     * @return string|null
     */
    public function render()
    {
        $this->addScript();

        return view($this->view, $this->variables())->render();
    }
}
