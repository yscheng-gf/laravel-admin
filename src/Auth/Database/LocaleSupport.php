<?php

namespace Xn\Admin\Auth\Database;

use Xn\Admin\Traits\DefaultDatetimeFormat;
use Illuminate\Database\Eloquent\Model;
use Xn\Admin\Helper\XNCache;

class LocaleSupport extends Model
{
    use DefaultDatetimeFormat;

    protected $fillable = ['code', 'default_name', 'status', 'sort_order'];

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $connection = config('admin.database.connection') ?: config('database.default');

        $this->setConnection($connection);

        $this->setTable(config('admin.database.locale_table'));

        parent::__construct($attributes);
    }

    protected static function boot()
    {
        parent::boot();

        self::saving(function ($model) {
            XNCache::LocaleClean();
        });
    }
}
