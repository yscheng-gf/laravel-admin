<?php

namespace Xn\Admin\Auth\Database;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\TranslationLoader\LanguageLine;

class LanguageLineEx extends LanguageLine
{
    use HasFactory;

    protected $table = 'language_lines';

    protected $casts = [
        'text' => 'json',
    ];
}
