<?php

namespace Xn\Admin\Controllers;

use Xn\Admin\Form;
use Xn\Admin\Grid;
use Xn\Admin\Show;

class LocaleSupportController extends AdminController
{
    /**
     * {@inheritdoc}
     */
    protected function title()
    {
        return trans('Locale supports');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $localModel = config('admin.database.locale_model');

        $grid = new Grid(new $localModel());

        $grid->column('id', 'ID')->sortable();
        $grid->column('code', trans('admin.code'));
        $grid->column('name', trans('admin.name'));
        $grid->column('status', trans('admin.status'))->switch(static::switchLocalize());
        $grid->column('sort_order', trans('admin.order'))->editable();

        $grid->column('created_at', trans('admin.created_at'));
        $grid->column('updated_at', trans('admin.updated_at'));

        $grid->model()->orderBy('id');

        $grid->tools(function (Grid\Tools $tools) {
            $tools->batch(function (Grid\Tools\BatchActions $actions) {
                $actions->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        $localModel = config('admin.database.locale_model');

        $show = new Show($localModel::findOrFail($id));

        $show->field('id', 'ID');
        $show->field('code', trans('admin.code'));
        $show->field('name', trans('admin.name'));
        $show->field('status', trans('admin.status'));
        $show->field('sort_order', trans('admin.order'));
        $show->field('created_at', trans('admin.created_at'));
        $show->field('updated_at', trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {

        $localModel = config('admin.database.locale_model');

        $form = new Form(new $localModel());

        $form->display('id', 'ID');

        $form->text('code', trans('admin.code'))->rules('required');
        $form->text('name', trans('admin.name'))->rules('required');
        $form->switch('status', trans('admin.status'))->states(static::switchLocalize());
        $form->text('sort_order', trans('admin.order'))->inputmask(['alias' => 'integer'])->rules('required');

        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        return $form;
    }
}
