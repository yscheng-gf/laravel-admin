<?php

namespace Xn\Admin\Controllers;

use Illuminate\Support\Facades\DB;
use Xn\Admin\Auth\Database\LanguageLineEx;
use Xn\Admin\Form;
use Xn\Admin\Grid;
use Xn\Admin\Helper\XNCache;
use Xn\Admin\Show;

/**
 * 翻译字典档
 */
class LanguageLineController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected function title()
    {
        return trans('Language lines');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $llModel = config('admin.database.languageline_model');

        $grid = new Grid(new $llModel);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('group', __('Group'))->sortable();
        $grid->column('key', __('Key'))->sortable();
        // $grid->column('created_at', __('Created at'));
        // $grid->column('updated_at', __('Updated at'));
        // filter
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->column(1/2, function ($filter) {
                $filter->like('group', __('Group'));
            });
            $filter->column(1/2, function ($filter) {
                $filter->like('key', __('Key'));
            });

        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $llModel = config('admin.database.languageline_model');

        $show = new Show($llModel::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $llModel = config('admin.database.languageline_model');
        $llTable = config('admin.database.languageline_table');

        $localModel = config('admin.database.locale_model');

        $form = new Form(new $llModel);
        $form->hidden('id');
        $form->text('group', __('Group'))->rules('required');
        $form->text('key', __('Key'))->rules("required|unique:{$llTable},key,".request()->input('id',0).',id,group,'.request()->input('group'));
        $form->keyValue('text', __('Text'))->value(
            XNCache::LocaleInputs()
        );

        return $form;
    }
}
