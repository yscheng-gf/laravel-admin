<?php

namespace Xn\Admin\Controllers\Traits;


use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Illuminate\Support\Facades\DB;
use Xn\Admin\Auth\Database\Administrator;

trait LINENotify
{
    /**
     * LINE-Notify 取消服務訊息通知
     *
     * @return void
     */
    public function lineNotifyCancel() {
        $username = request()->get('username');
        $admin = Administrator::where(['username'=>$username])->first();
        # 若使用者已連動則進行取消連動作業
        if (!empty($admin['line_notify_token'])) {
            $this->lineNotifyRevoke($username, $admin);
            session()->flash('status', __('admin.update_succeeded'));
            return redirect()->route('admin.setting');
        }

        return redirect()->route('admin.setting');
    }

    /**
     * 註冊服務訊息通知
     *
     * @param [type] $store_id
     * @param [type] $user_id
     * @return void
     */
    public function lineNotifyCallback() {
        $username = request()->get('username');
        $code = request()->get('code');
        $callbackUrl = route('admin.line-notify.callback', ['username' => $username]);
        DB::statement("update admin_users set line_notify_auth_code='{$code}' where username='{$username}'");
        ### LINE Access Token ###
        $this->getNotifyAccessToken($username, $code, $callbackUrl);
        session()->flash('status', __('admin.update_succeeded'));
        return redirect()->route('admin.setting');
    }

    /**
     * 取消服務通知
     *
     * @param [type] $access_token
     * @return void
     */
    public function lineNotifyRevoke($username, $admin) {
        $client = new Client();
        $client->request('POST', 'https://notify-api.line.me/api/revoke', [
            'headers' => [
                'Authorization'=> 'Bearer ' . $admin['line_notify_token']
            ],
            'on_stats' => function(TransferStats $stats) use($username) {
                if ($stats->hasResponse()) {
                    $code =  $stats->getResponse()->getStatusCode();
                    if (in_array($code,[200,401])) {
                        $adminTable = config('admin.database.users_table');
                        DB::statement("update $adminTable set line_notify_token = null where username='{$username}'");
                    }
                } else {
                    // Error data is handler specific. You will need to know what
                    // type of error data your handler uses before using this
                    // value.
                    var_dump($stats->getHandlerErrorData());
                }
            }
        ]);
    }

    /**
     * 取得LINE Notify Access Token
     *
     * @param [type] $store_id
     * @param [type] $user_id
     * @param [type] $code
     * @param [type] $redirect_uri
     * @return void
     */
    private function getNotifyAccessToken($username, $code, $redirect_uri) {
        $params = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $redirect_uri,
            'client_id' => config('admin.notify.line.client_id'),
            'client_secret' => config('admin.notify.line.client_secret')
        ];
        $client = new Client();
        $client->request('POST', 'https://notify-bot.line.me/oauth/token', [
            'headers' => [
                'Content-Type'=> 'application/x-www-form-urlencoded'
            ],
            'form_params' => $params,
            'on_stats' => function(TransferStats $stats) use($username) {
                if ($stats->hasResponse()) {
                    $output =  $stats->getResponse()->getBody();
                    /**
                     * {
                     *      "status": 200,
                     *      "message": "access_token is issued",
                     *      "access_token": "7giNDfFWoAO1trYBA34YyfA6IZmazQoF4rmWSqrTtb3"
                     *  }
                     */
                    $result = json_decode($output, true);
                    $token = $result['access_token'];
                    DB::statement("update admin_users set line_notify_token='{$token}' where username='{$username}'");
                } else {
                    // Error data is handler specific. You will need to know what
                    // type of error data your handler uses before using this
                    // value.
                    var_dump($stats->getHandlerErrorData());
                }
            }
        ]);
    }
}
