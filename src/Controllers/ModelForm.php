<?php

namespace Xn\Admin\Controllers;

/**
 * Trait ModelForm.
 *
 * @deprecated Use `HasResourceActions` instead.
 */
trait ModelForm
{
    use HasResourceActions;
}
