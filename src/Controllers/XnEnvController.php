<?php

namespace Xn\Admin\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class XnEnvController extends Controller
{
    /**
     * 切換時區
     *
     * @param Request $request
     * @return void
     */
    public function switchTimezone(Request $request) {
        $timezone = $request->input('timezone');
        session(['timezone' => $timezone]);
        //
        return response()->json([
            'status' => 'ok'
        ], 200);
    }

    /**
     * 切換語系
     *
     * @param Request $request
     * @return void
     */
    public function switchLocale(Request $request) {
        $locale = $request->input('locale');
        session(['locale' => $locale]);

        return response()->json([
            'status' => 'ok'
        ], 200);
    }
}
