<?php

namespace Xn\Admin\Form\Field;

use Xn\Admin\Facades\Admin;

class PasswordMeter extends Text
{

    protected $view = 'admin::form.password-meter';

    protected static $css = [
        'vendor/laravel-admin/password-meter/password-meter.css'
    ];

    protected static $js = [
        'vendor/laravel-admin/password-meter/zxcvbn.js',
        'vendor/laravel-admin/password-meter/password-meter.js'
    ];

    protected function addScript()
    {
        $script = <<<SCRIPT
        $('#{$this->id}').bootstrapPasswordMeter({minPasswordLength:1});
SCRIPT;

        Admin::script($script);
    }

    public function render()
    {
        $this->addScript();
        $this->prepend('<i class="fa fa-eye-slash fa-fw"></i>')
            ->defaultAttribute('type', 'password');

        return parent::render();
    }
}
