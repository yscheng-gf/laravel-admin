<?php

namespace Xn\Admin\Form\Field;

use Illuminate\Contracts\Support\Arrayable;
use Xn\Admin\Form\Field\Text;

class Selectize extends Text
{
    protected $view = 'admin::form.selectize';

    protected static $css = [
        '/vendor/laravel-admin/selectize/selectize.bootstrap3.css',
    ];

    protected static $js = [
        '/vendor/laravel-admin/selectize/selectize.min.js',
    ];

    /**
     * @var array
     */
    protected $config = [
        'delimiter' => ',',
        'persist' => false,
    ];

    /**
     * Selectize Plugins
     *
     * @var array
     */
    protected $plugins = ['restore_on_backspace', 'remove_button'];

    /**
     * Set options.
     *
     * @param array|callable|string $options
     *
     * @return $this|mixed
     */
    public function options($options = [])
    {

        // remote options
        if (is_string($options)) {
            return $this->loadRemoteOptions(...func_get_args());
        }

        if ($options instanceof Arrayable) {
            $options = $options->toArray();
        }

        if (is_callable($options)) {
            $this->options = $options;
        } else {
            $this->options = (array) $options;
        }

        return $this;
    }

    /**
     * @param array $groups
     */

    /**
     * optgroup columns
     *
     * @param array $options
     * @param array $groups
     * @param array $params
     * @return void
     */
    public function optgroupColumns(array $options, array $groups, $params = [])
    {
        $this->plugins[] = 'optgroup_columns';
        /**
         * 資料格式
         * options:[
         *      {id: 'avenger', group: 'dodge', label: 'Avenger'},
         *      {id: 'caliber', group: 'dodge', label: 'Caliber'},
         *      {id: 'caravan-grand-passenger', group: 'dodge', label: 'Caravan Grand Passenger'},
         *      {id: 'challenger', group: 'dodge', label: 'Challenger'},
         * ],
         * optgroups:[
         *      {id: 'dodge', label: 'Dodge'},
         *      {id: 'audi', label: 'Audi'},
         *      {id: 'chevrolet', label: 'Chevrolet'}
         * ]
         */
        $config = [
            'options'               => $options,
            'optgroups'             => $groups,
            'labelField'            => $params['labelField']??'label',
            'valueField'            => $params['valueField']??'id',
            'optgroupField'         => $params['optgroupField']??'group',
            'optgroupLabelField'    => $params['optgroupLabelField']??'label',
            'optgroupValueField'    => $params['optgroupValueField']??'id',
            'optgroupOrder'         => array_column($groups, $params['optgroupValueField']??'id'),
            'searchField'           => $params['searchField']??'label'
        ];
        $this->configs = array_merge($this->configs, $config);
        return $this;
    }

    /**
     * Set config for selectize.
     *
     * all configurations see https://github.com/selectize/selectize.js/blob/master/docs/api.md
     *
     * @param string $key
     * @param mixed  $val
     *
     * @return $this
     */
    public function config($key, $val)
    {
        $this->config[$key] = $val;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function render()
    {

        if ($this->options instanceof \Closure) {
            if ($this->form) {
                $this->options = $this->options->bindTo($this->form->model());
            }

            $this->options(call_user_func($this->options, $this->value));
        }

        $oldValues = is_array($this->value) ? $this->value : explode($this->config['delimiter'], $this->value);

        $optons = [];

        if (is_array($this->options)) {
            foreach($this->options as $ix => $val){
                if (empty($val)) continue;
                $optons[] = [
                    'code' => $ix,
                    'name' => $val
                ];
            }
        }

        foreach($oldValues as $val){
            if (empty($val)) continue;
            $optons[] = [
                'code' => $val,
                'name' => $val
            ];
        }

        $configs = array_merge([
            'plugins'   => $this->plugins,
            'valueField' => 'code',
            'labelField' => 'name',
            'searchField' => ['code', 'name'],
            'options'   => $optons,
            'create'    => "function(ipnut){return {value:input, text:input}}"
        ], $this->config);

        $configs = json_encode($configs);

        if (empty($this->script)) {
            $this->script = "myselectize = $(\"{$this->getElementClassSelector()}\").selectize($configs);";
        }

        return parent::render();
    }
}
