<?php

namespace Xn\Admin\Form\Field;

use Closure;
use Xn\Admin\Facades\Admin;
use Xn\Admin\Form\Field\Display;

class OtpQRCode extends Display
{
    protected $view = 'admin::form.otp-qrcode';

    protected static $css = [
    ];

    protected static $js = [
    ];

    protected $qrCodeLabel;

    /**
     * 顯示標籤
     *
     * @param [type] $label
     * @return $this
     */
    public function setQrCodeLabel($label)
    {
        $this->qrCodeLabel = $label;

        return $this;
    }

    /**
     * 產出QRCode
     *
     * @param string $userName
     * @return $this
     */
    protected function genQRCode()
    {
        $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
        $this->value = $google2fa->getQRCodeInline(
            env('APP_NAME') . " ". env('APP_ENV'),
            $this->qrCodeLabel,
            $this->value,
            120
        );
        return $this;
    }

    /**
     * Render this filed.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function render()
    {
        if (!$this->shouldRender()) {
            return '';
        }

        $this->addRequiredAttributeFromRules();

        if ($this->callback instanceof Closure) {
            $this->value = $this->callback->call($this->form->model(), $this->value, $this);
        }

        $this->genQRCode();

        Admin::script($this->script);

        return Admin::component($this->getView(), $this->variables());
    }
}
