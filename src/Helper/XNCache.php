<?php

namespace Xn\Admin\Helper;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Xn\Admin\Auth\Database\LocaleSupport;
use Xn\Admin\Auth\Database\TimeZone;

class XNCache {

   static $oneDay = 3600 * 24;

   static $oneMinute = 1 * 60;

   /**
    * 建立交易DB連線
    *
    * @param string $dbName
    * @param string $dbConfig
    * @return string
    */
    public static function CreateTxDbConfig($dbConfig, $dbName = 'postgres') : string {
        $db = json_decode($dbConfig, true);
        $connName = ($dbName == 'postgres') ? $db['ip'] : $dbName;
        Config::set("database.connections.{$connName}", [
            'driver' => 'pgsql',
            'url' => null,
            'host' => $db['ip'],
            'port' => $db['port'],
            'database' => $dbName,
            'username' => $db['user'],
            'password' => $db['password'],
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ]);
        return $connName;
    }

    /**
     * 建立MongoDB連線
     *
     * @param string $dbConfig
     * @param string $dbName
     * @return string
     */
    public static function CreateMongoDbConfig($dbConfig, $dbName) : string {
        return static::CreateRepDbConfig($dbConfig, $dbName);
    }

   /**
    * 建立報表DB連線
    *
    * @param string $dbName
    * @param string $dbConfig
    * @return string
    */
    public static function CreateRepDbConfig($dbConfig, $dbName) : string {
        $db = json_decode($dbConfig, true);
        $connName = $dbName;
        Config::set("database.connections.{$connName}", [
            'driver' => 'mongodb',
            'host' => explode(",", $db['ip']),
            'port' => env('MONGODB_PORT', 27017),
            'database' => $dbName,
            'username' => "",
            'password' => "",
            'options' => [
                'replicaSet' => $db['rs'],
                'readPreference' => 'secondary',
                'database' => env('MONGODB_AUTHENTICATION_DATABASE', 'admin'), // required with Mongo 3+
            ],
        ]);
        return $connName;
    }

    /**
     * 檢查IP是否在
     *
     * @param $src
     * @param array $list
     * @return bool
     */
    public static function IpContainChecker($src, array $list): bool {
        $src = is_array($src) ? current($src) : $src;
        $src = str_ireplace(' ', '', $src);
        $ips = explode(',', $src);
        foreach ($ips as $ip) {
            if (in_array($ip, $list)) {
                return true;
            }
            foreach($list as $slashIp) {
                if ( strpos( $slashIp, '/' ) !== false ) {
                    if (ip_in_range($ip, $slashIp)===true) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * 多語系for keyValue 套件
     *
     * @return array
     */
    public static function LocaleInputs() {
        $localModel = config('admin.database.locale_model');
        return $localModel::select('code', DB::raw("''as lang"))
            ->where('status', '1')->orderBy('sort_order')->orderBy('id')->pluck('lang', 'code')->toArray();
    }

    /**
     * 時區
     *
     * @return array
     */
    public static function Timezones() {
        $localModel = config('admin.database.timezone_model');
        return Cache::remember('timezones', static::$oneDay, function() use ($localModel) {
            return $localModel::select('name','timezone')->where('status', "1")
            ->orderBy('sort_order', 'desc')->get()->toArray();
        });
    }

    /**
     * 語系
     *
     * @return array
     */
    public static function Locales() {
        $localModel = config('admin.database.locale_model');
        return Cache::remember('locales', static::$oneDay, function() use ($localModel) {
            return $localModel::select('code','name')->where('status', "1")
            ->orderBy('sort_order', 'desc')->pluck('name', 'code');
        });
    }

    /**
     * 語系
     *
     * @return object
     */
    public static function LocaleArray() {
        $localModel = config('admin.database.locale_model');
        return Cache::remember('locale_array', static::$oneDay, function() use ($localModel) {
            return $localModel::select('code','name')->where('status', "1")->get();
        });
    }

    /**
     * 清除 語系
     *
     * @return void
     */
    public static function LocaleClean() {
        Cache::forget("locales");
        Cache::forget("locale_array");
    }

    /**
     * 清除時區
     *
     * @return void
     */
    public static function TimezoneClean() {
        Cache::forget("timezones");
    }

    /**
     * Null 转 “”
     *
     * @param [type] $data
     * @return void
     */
    public static function NullToEmpty(&$data) {
        $_data = json_encode($data);
        $data = json_decode(preg_replace("/null/i", '""', $_data), true);
    }

    /**
     * 清除所有語系
     *
     * @param [type] $prefix
     * @return void
     */
    public static function _CleanAllLang($prefix) {
        $localModel = config('admin.database.locale_model');
        foreach($localModel::all() as $lang) {
            Cache::forget("{$prefix}_{$lang['code']}");
            Redis::del("{$prefix}_{$lang['code']}");
        }
    }
}

?>
