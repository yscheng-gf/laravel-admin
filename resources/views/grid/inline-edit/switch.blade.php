<input type="checkbox" class="{{ $class }}" {{ $checked }} data-key="{{ $key }}" />
<script>
    $('.{{ $class }}').bootstrapSwitch({
        size:'mini',
        onText: '{{ $states['on']['text'] }}',
        offText: '{{ $states['off']['text'] }}',
        onColor: '{{ $states['on']['color'] }}',
        offColor: '{{ $states['off']['color'] }}',
        onSwitchChange: function(event, state){
            @if ($confirm??false)
                swal.fire({
                    title: "{{$trans['title']}}",
                    icon: 'warning',
                    confirmButtonText: "{{$trans['confirm']}}",
                    cancelButtonText: "{{$trans['cancel']}}",
                    showLoaderOnConfirm: true,
                    showCancelButton: true,
                    allowOutsideClick: false,
                }).then((result) => {
                    if (result.value) {
                        $(this).val(state ? 'on' : 'off');

                        var key = $(this).data('key');
                        var value = $(this).val();
                        var _status = true;

                        $.ajax({
                            url: "{{ $resource }}/" + key,
                            type: "POST",
                            async:false,
                            data: {
                                "{{ $name }}": value,
                                _token: LA.token,
                                _method: 'PUT',
                                _edit_inline: true,
                            },
                            success: function (data) {
                                if (data.status){
                                    toastr.success(data.message);
                                    $.pjax.reload('#pjax-container');
                                }
                                else
                                    toastr.warning(data.message);
                            },
                            complete:function(xhr,status) {
                                if (status == 'success')
                                    _status = xhr.responseJSON.status;
                            }
                        });

                        return _status;
                    }
                });
            @else
                $(this).val(state ? 'on' : 'off');

                var key = $(this).data('key');
                var value = $(this).val();
                var _status = true;

                $.ajax({
                    url: "{{ $resource }}/" + key,
                    type: "POST",
                    async:false,
                    data: {
                        "{{ $name }}": value,
                        _token: LA.token,
                        _method: 'PUT',
                        _edit_inline: true,
                    },
                    success: function (data) {
                        if (data.status)
                            toastr.success(data.message);
                        else
                            toastr.warning(data.message);
                    },
                    complete:function(xhr,status) {
                        if (status == 'success')
                            _status = xhr.responseJSON.status;
                    }
                });

                return _status;
            @endif
            return false;
        }
    });
</script>
