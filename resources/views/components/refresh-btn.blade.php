<li>
    <div class="btn-group navbar-btn">
        <div type="button" class="btn container-refresh" href="javascript:void(0);" style="padding-left: 6px;padding-right: 6px;">
            <i class="fas fa-sync-alt"></i>
        </div>
        @if (\Admin::user()->inRoles(config('admin.refresh_timer.roles', [])))
            <div type="button" class="btn dropdown-toggle" style="padding-left: 4px;padding-right: 4px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </div>
            <ul class="dropdown-menu">
                @foreach (config('admin.refresh_timer.timers', []) as $timer )
                    <li><a href="#" class="setTimer" data-secs="{{$timer}}">{!! $timer !!} {{__('secs')}}</a></li>
                @endforeach
                <li role="separator" class="divider"></li>
                <li><a href="#" class="setTimer" data-secs="0">{{__('admin.close')}}</a></li>
            </ul>
            <span class="label label-info" id="timer-sec">0</span>
        @endif
    </div>
</li>
<script>
    $('.container-refresh').off('click').on('click', function() {
        $.admin.reload();
        $.admin.toastr.success('{{ __('admin.refresh_succeeded') }}', '', {positionClass:"toast-top-center"});
    });
    //
    function setRefreshTimer() {
        if (!isNaN(parseInt(localStorage.getItem('refreshTimer')))&&window._refreshTimerDowncountInterval == undefined) {
            var secs = parseInt(localStorage.getItem('refreshTimer'));
            $('#timer-sec').text(secs);
            window._refreshTimerDowncountInterval = setInterval(refreshTimerDowncount, 1000);
            window._refreshTimerInterval = setInterval(refreshTimer, secs * 1000);
        }
    }

    $('body').off('click', '.setTimer');
    $('body').on('click', '.setTimer', function(){
        var secs = parseInt($(this).data('secs'));
        $('#timer-sec').text(secs);
        if (secs > 0) {
            localStorage.setItem('refreshTimer', secs);
            localStorage.setItem('refreshTimerDate', new Date());
        } else {
            localStorage.removeItem('refreshTimer');
            localStorage.removeItem('refreshTimerDate');
            clearInterval(window._refreshTimerDowncountInterval);
            clearInterval(window._refreshTimerInterval);
            window._refreshTimerDowncountInterval = undefined;
            window._refreshTimerInterval = undefined;
        }
        setRefreshTimer();
    });
    //
    window.refreshTimerDowncount = function(){
        var secs = parseInt($('#timer-sec').text());
        if (!isNaN(secs) && secs > 0) {
            secs--;
            $('#timer-sec').text(secs);
        }
    }
    //
    window.refreshTimer = function(){
        var secs = parseInt(localStorage.getItem('refreshTimer'));
        if (!isNaN(secs)) {
            var timerDate = new Date(localStorage.getItem('refreshTimerDate'));
            if (((new Date()).getTime() - timerDate.getTime())/1000 > 0 ) {
                swal.fire({
                    title: '<div class="fa-2x"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></div>',
                    showCancelButton: false,
                    showConfirmButton: false
                });
                setTimeout(function(){
                    swal.close();
                    $.pjax.reload('#pjax-container');
                    localStorage.setItem('refreshTimerDate', new Date());
                    var secs = parseInt(localStorage.getItem('refreshTimer'));
                    secs--;
                    $('#timer-sec').text(secs);
                },1000);
            }
        }
    }
    //
    setRefreshTimer();
</script>

<style>
    .nav li .btn-group .label {
        position: absolute;
        top: 1px;
        right: 7px;
        text-align: center;
        font-size: 9px;
        padding: 2px 3px;
        line-height: .9;
    }

    .nav li:hover {
        background: rgba(0, 0, 0, 0.1);
    }

    body:not(.skin-black-light) .navbar-btn, body:not(.skin-black-light) .navbar-btn > [type="button"]:hover {
        color: white;
    }
</style>

{{-- <li>
    <a href="javascript:void(0);" class="container-refresh">
        <i class="fas fa-sync-alt"></i>
    </a>
</li>
<script>
    $('.container-refresh').off('click').on('click', function() {
        $.admin.reload();
        $.admin.toastr.success('{{ __('admin.refresh_succeeded') }}', '', {positionClass:"toast-top-center"});
    });
</script> --}}
